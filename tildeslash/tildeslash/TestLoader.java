
package tildeslash;


/**
 *  Load and instanciate a class. TestLoader is defined here, to make
 *  a dependency, so the dependency support in mmake can be tested.
 */
class TestLoader extends Thread
{
  private String aClass;
  
  public TestLoader(String c)
  {
    aClass= c;
    start();
  }
  
  public void run()
  {  
    try {
      Class t= Class.forName(aClass);
      t.newInstance();
    } catch (Exception e){System.out.println("Error: "+e.getMessage());};
  }

}

