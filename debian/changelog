mmake (2.3-10) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.6.0 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Fix field name case in debian/copyright (Upstream-name ⇒ Upstream-Name).
  * Marked binary package with perl script as Multi-Arch: foreign.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 20 May 2024 10:53:06 +0200

mmake (2.3-9) unstable; urgency=medium

  * QA upload.
  * debian/control:
      - Added Rules-Requires-Root: no.
      - Bumped Standards-Version to 4.6.0.
      - Updated Depends field to use "perl:any" to solve a Multiarch hinter.
      - Updated Homepage field to use https.
  * debian/copyright:
      - Added packaging rights for myself and Boyuan Yang.
      - Updated Format field to use https protocol.
      - Updated Upstream-Source field to use https.
  * debian/lintian-overrides: added override for a maintainer name.
  * debian/patches:
      - Renamed mmake-output-file.patch to 010_mmake-output-file.patch.
      - Renamed fix-manpage.patch to 020_fix-manpage.patch.
  * debian/salsa-ci.yml: created to provide CI tests for Salsa
  * debian/tests/control: created to perform a trivial CI test.
  * debian/upstream/metadata: created.
  * debian/watch: updated protocol to https.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Thu, 21 Apr 2022 12:05:15 -0300

mmake (2.3-8) unstable; urgency=medium

  * QA upload.
  * Bump debhelper compat to v13.
  * Bump Standards-Version to 4.5.1.
  * Source-only upload.

 -- Boyuan Yang <byang@debian.org>  Thu, 31 Dec 2020 14:20:58 -0500

mmake (2.3-7) unstable; urgency=medium

  * QA upload.
  * Migrated to DebSrc 3.0.
  * debian/clean:
      - Created to remove Makefile after building.
  * debian/control:
      - Update DH level from 4 to 9. (Closes: #817585)
      - Added Homepage field.
      - Added the ${misc:Depends} variable to provide the right install
        dependencies.
      - Bumped Standards-Version to 3.9.8.
      - Set correct priority to optional.
      - Using dh-autoreconf now.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all information.
  * debian/dirs: unnecessary. Removed.
  * debian/patches (quilt):
      - mmake_2.3-6.diff.gz splitted:
          -mmake-output-file.diff:
             ~ Renamed to mmake-output-file.patch.
             ~ Added a header.
          -fix-manpage.patch:
             ~ fixes a few typo in manpage.
          -all the Makefile file was being provided by patch until previous
           revision. Now, using dh-autoreconf, it's being generated
           at build time
  * debian/rules:
      - Updated to new (reduced) format.
      - Created override_dh_auto_install to fix Makefile patch.
  * debian/watch:
      - Bumped to version 4.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 02 Jul 2016 13:33:20 -0300

mmake (2.3-6) unstable; urgency=high

  * QA upload
  * Set Maintainer to Debian QA Group
  * Change dependency on perl5 to perl following the removal of
    the former (Closes: #808220)

 -- Dominic Hargreaves <dom@earth.li>  Thu, 17 Dec 2015 11:47:57 +0000

mmake (2.3-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control (Build-Depends): Replaced jikes with default-jdk.
    (Recommends): Likewise, closes: #528065.

 -- Michael Banck <mbanck@debian.org>  Sun, 29 Nov 2009 11:51:00 +0100

mmake (2.3-5) unstable; urgency=low

  * mmake.1: Properly escape "-" characters in invocation synopsis since
    they should be minus signs, not hyphens.

  * debian/copyright: Give proper copyright notice.

 -- Thomas Bushnell, BSG <tb@debian.org>  Sun, 24 Feb 2008 18:55:32 -0500

mmake (2.3-4) unstable; urgency=low

  * debian/control (Standards-Version): Update to 3.7.3.  No changes needed.

  * debian/rules (clean): Don't ignore all errors on Make clean invocation.
    Fixes lintian warning debian-rules-ignores-make-clean-error.

 -- Thomas Bushnell, BSG <tb@debian.org>  Mon, 14 Jan 2008 16:11:11 -0500

mmake (2.3-3) unstable; urgency=low

  * debian/control (Standards-Version): Update to 3.7.2.

  * debian/control (Build-Depends-Indep): Remove debhelper.
  (Build-Depends): Add debhelper.

  * debian/control: Add final newline.

 -- Thomas Bushnell, BSG <tb@debian.org>  Tue,  3 Oct 2006 00:04:55 -0700

mmake (2.3-2) unstable; urgency=low

  * debian/copyright: Update FSF address.

  * debian/control (Standards-Version): Now 3.6.2.

  * debian/compat: New file; now 4.
  * debian/rules (install): Install into debian/mmake.

 -- Thomas Bushnell, BSG <tb@debian.org>  Fri, 30 Sep 2005 22:33:22 -0700

mmake (2.3-1) unstable; urgency=low

  * New upstream release.

  * debian/watch: New file.

 -- Thomas Bushnell, BSG <tb@debian.org>  Fri, 30 Sep 2005 21:34:54 -0700

mmake (2.2.1-6) unstable; urgency=low

  * Actually set new maintainer.  Whoops.

 -- Thomas Bushnell, BSG <tb@debian.org>  Wed, 18 Aug 2004 11:35:51 -0700

mmake (2.2.1-5) unstable; urgency=low

  * New maintainer.  Closes: #261581.

 -- Thomas Bushnell, BSG <tb@debian.org>  Wed, 18 Aug 2004 01:19:52 -0700

mmake (2.2.1-4) unstable; urgency=low

  * QA upload.
  * Made depends on findutils versioned, closes: #216534.
  * Updated standards version from 3.0.1 to 3.6.1.
  * Updated copyright file so it include GPL in a correct way, closes: #255955.
  * Applied patch from Matt Zimmerman <mdz@debian.org> to allow output
    of other file than Makefile, closes: #199875.
  * Removed emacs variables from bottom of this changelog.
  * Removed dh_make examples from debian dir.
  * Made build depends indep instead of build depends.
  * Changed recommends from java-compiler to jikes | java-compiler.
  * Removed dh_suidregister from debian/rules.

 -- Ola Lundqvist <opal@debian.org>  Tue, 17 Aug 2004 09:43:19 +0200

mmake (2.2.1-3) unstable; urgency=low

  * Orphaned this package.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 26 Jul 2004 15:01:52 -0500

mmake (2.2.1-2) unstable; urgency=low

  * Added build-dep for debhelper.  Closes: #133704.

 -- John Goerzen <jgoerzen@complete.org>  Wed, 13 Feb 2002 09:15:10 -0500

mmake (2.2.1-1) unstable; urgency=low

  * New upstream release.
  * Added dependency on findutils for xargs.

 -- John Goerzen <jgoerzen@complete.org>  Wed, 18 Jul 2001 11:21:41 -0500

mmake (1.32-2) unstable; urgency=low

  * Eliminated duplicate manpage.  Closes: #56318.

 -- John Goerzen <jgoerzen@complete.org>  Sun, 13 Feb 2000 19:07:46 -0600

mmake (1.32-1) unstable; urgency=low

  * New upstream release
  * Updated Standards-Version
  * Now FHS.
  * Changed Recommends and Depends.  Closes: #44465.
  * Closed out old bug: #41470.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 21 Sep 1999 13:15:01 -0500

mmake (1.17-1.1) unstable; urgency=low

  * NMU for the perl upgrade. Closes: #41470
  * Corrected the perl dependency.
  * Upgraded standards-version to 2.5.1.
  * Removed the empty README.debian file.

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Thu, 22 Jul 1999 20:32:23 +0200

mmake (1.17-1) unstable; urgency=low

  * New upstream release; has performance enhancements with recursion.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 27 Oct 1998 09:48:50 -0600

mmake (1.16-1) unstable; urgency=low

  * New upstream release
  * Updated rules:clean to not halt if files trying to be cleaned
    aren't there.

 -- John Goerzen <jgoerzen@complete.org>  Sat, 17 Oct 1998 18:42:37 -0500

mmake (1.9-1) unstable; urgency=low

  * Initial Release.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 12 Oct 1998 12:10:22 -0500
